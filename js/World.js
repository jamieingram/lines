function World (container_el) {

  this.container_el = container_el;
  this.camera;
  this.scene;
  this.raycaster;
  this.mouseX = this.mouseY = 0;
  this.mouse = new THREE.Vector2();
  this.lines = [];
  this.activeLine;
  this.particleParents = {};
  this.selectedParticle;
  this.init();
  
}

World.particles = {};
 
World.prototype.init = function() {
  this.camera = new THREE.PerspectiveCamera(75, window.innerWidth / 740, 1, 4000);
  this.camera.position.x = 500;
  this.camera.position.z = 1000;
  this.camera.rotation.z = 30 * Math.PI / 180;
  this.camera.rotation.y = 10 * Math.PI / 180;

  this.scene = new THREE.Scene();

  this.plane = new THREE.Mesh(
    new THREE.PlaneBufferGeometry( 4000, 4000, 8, 8 ),
    new THREE.MeshBasicMaterial({color: 0x000000, opacity: 0.25, transparent: true})
  );
  this.plane.name = "plane";
  this.plane.visible = false;
  this.scene.add(this.plane);
  //
  //this.renderer = new THREE.CanvasRenderer();
  this.renderer = new THREE.WebGLRenderer();
  this.renderer.sortObjects = false;
  this.renderer.setPixelRatio(window.devicePixelRatio);
  this.renderer.setSize(window.innerWidth, 740);
  this.renderer.setClearColor('#78dcc4');
  this.renderer.clear();
  this.container_el.append(this.renderer.domElement);
  //
  this.raycaster = new THREE.Raycaster();

  var xPos = -2500;
  var numLines = 20;
  var width = 4500;
  var inc = width / numLines;
  //
  for (var i = 0 ; i < numLines ; i++) {
    var line = new SegmentedLine();
    line.setPosition(xPos, 0, 0);
    this.scene.add(line.getSceneObject());
    for (var j = 0 ; j < line.getSceneObject().children.length ; j++) {
      var particle = line.getSceneObject().children[j];
      this.particleParents[particle.uuid] = line;
    }
    this.lines.push(line);
    xPos += (inc + ((Math.random() * 200) - 100));
  }
  //
  $(this.container_el).on('mousemove', $.proxy(this.onSceneMouseMove, this));
  $(this.container_el).on('mousedown', $.proxy(this.onSceneMouseDown, this));
  $(this.container_el).on('mouseup', $.proxy(this.onSceneMouseUp, this));

  window.addEventListener('resize', $.proxy(this.onWindowResize, this), false);

};

World.prototype.onWindowResize = function(event) {
  this.camera.aspect = window.innerWidth / 740;
  this.camera.updateProjectionMatrix();
  this.renderer.setSize( window.innerWidth,740);
}

World.prototype.onSceneMouseMove = function (event) {
  event.preventDefault();

  this.mouse.x = ( event.clientX / window.innerWidth ) * 2 - 1;
  this.mouse.y = - ( event.clientY / 740 ) * 2 + 1;

  if (this.selectedParticle != undefined) {
    var intersects = this.raycaster.intersectObject(this.plane);
    this.selectedParticle.position.copy(intersects[0].point.sub(offset));
  }
}

World.prototype.onSceneMouseDown = function (event) {
  event.preventDefault();
  var vector = new THREE.Vector3(this.mouse.x,this.mouse.y, 0.5).unproject(this.camera);
  var raycaster = new THREE.Raycaster(this.camera.position, vector.sub(this.camera.position).normalize());
  raycaster.setFromCamera(this.mouse,this.camera);
  // calculate objects intersecting the picking ray
  var intersects = raycaster.intersectObjects(this.scene.children, true);
  for (var i = 0 ; i < intersects.length ; i++) {
    if (intersects[i].object.type == "Mesh" && intersects[i].object.name != "plane") {
      var uuid = intersects[i].object.uuid;
      var particle = World.particles[uuid];
      this.selectedParticle = particle;
      particle.mouseDown();
      //this.plane.position.copy(particle.getSceneObject().position);
      //this.plane.lookAt(this.camera.position);
      break;
    }
  }
}

World.prototype.onSceneMouseUp = function (event) {
  event.preventDefault();
  for (var i = 0 ; i < World.particles.length ; i++) {
    var particle = World.particles[i];
    if (particle.isMouseDown == true) {
      particle.mouseUp();
    }
  }
  this.selectedParticle = undefined;
}

World.prototype.checkIntersections = function() {
  //
  this.raycaster.setFromCamera(this.mouse,this.camera);
  // calculate objects intersecting the picking ray
  var intersects = this.raycaster.intersectObjects(this.scene.children, true);

  if (intersects.length > 1) {
    for (var i = 0 ; i < intersects.length ; i++) {
      if (intersects[i].object.type == "Mesh" && intersects[i].object.name != "plane") {
        var uuid = intersects[i].object.uuid;
        if (this.activeLine == null) {
          this.activeLine = uuid;
          var line = this.particleParents[uuid];
          line.rollOver(uuid);
          var particle = line.getParticleById(uuid);
          particle.rollOver();
          break;
        }
      }
    }
  } else {
    this.resetActiveLines();
  }
}

World.prototype.resetActiveLines = function() {
  for (var i = 0 ; i < this.lines.length ; i++) {
    var line = this.lines[i];
    if (line.active == true) line.rollOut();
  }
  this.activeLine = null;
}

World.prototype.render = function() {

  for (var i = 0 ; i < this.lines.length ; i++) {
    var line = this.lines[i];
    line.update();
  }
  //
  this.checkIntersections();
  //
  this.renderer.render(this.scene, this.camera);
}