(function($, Poke, undefined) {

    $(document).ready(function() {

        var container_el;
        var camera, scene, renderer, group, particle;

        container_el = $('.container');
        //
        //add stats
        var stats = new Stats();
        stats.domElement.style.position = 'absolute';
        stats.domElement.style.top = '0px';
        container_el.append(stats.domElement);

        var world = new World(container_el);

        function update() {
            requestAnimationFrame(update);
            TWEEN.update();
            render();
            stats.update();
        };

        function render() {
            world.render();
        }

        update();

    });

})(jQuery, window.Poke || (window.Poke = {}));
