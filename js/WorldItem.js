function WorldItem () {
  this.worldItem;
  this.initialPos;
  this.initialScale;
  this.moveTween;
}

WorldItem.prototype.getSceneObject = function() {
  return this.worldItem;
}

WorldItem.prototype.getInitialPos = function() {
  return this.initialPos;
}

WorldItem.prototype.setPosition = function (x, y, z) {
  this.worldItem.position.x = x;
  this.worldItem.position.y = y;
  this.worldItem.position.z = z;
  if (this.initialPos == undefined) {
    this.initialPos = {
      x:x,
      y:y,
      z:z
    }
  }
}

WorldItem.prototype.getPosition = function () {
  return {
    x:this.worldItem.position.x,
    y:this.worldItem.position.y,
    z:this.worldItem.position.z
  }
}

WorldItem.prototype.moveTo = function(x, y, z, speed, ease, completeFunc, self) {
  var speed = speed || 200;
  var ease = ease || TWEEN.Easing.Quadratic.Out;
  if (this.moveTween != undefined) this.moveTween.stop();
  this.moveTween = new TWEEN.Tween(this.worldItem.position);
  this.moveTween.to({x:x, y:y, z:z}, speed);
  this.moveTween.easing(ease);
  if (completeFunc != undefined) {
    this.moveTween.onComplete($.proxy(completeFunc, self));
  }else{
    this.moveTween.onComplete($.proxy(this.onMoveComplete, this));
  }
  this.moveTween.start();
}

WorldItem.prototype.onMoveComplete = function() {
  
}

WorldItem.prototype.setScale = function (scale) {
  if (this.initialScale == undefined) {
    this.initialScale = scale;
  }
  this.worldItem.scale.x = this.worldItem.scale.y = scale;
}