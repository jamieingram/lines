function SegmentedLine () {
  WorldItem.call(this);
  this.particles = {};
  this.particlePositions = {};
  this.line;
  this.xVelocity;
  this.originalXVelocity;
  this.xMin = -200;
  this.xMax = 200;
  this.counter = 0;
  this.movementInc = 0.01;
  this.active = false;
  this.init();
}

SegmentedLine.prototype = new WorldItem();
 
SegmentedLine.prototype.init = function() {
  //
  this.worldItem = new THREE.Group();
  var yPos = -2000;
  var xPos = Math.random() * 1000 - 500;
  //
  for (var i = 0 ; i < 8 ; i++) {
    var particle = new Particle(1.5);
    particle.setScale(30);
    xPos *= (100 - (Math.random() * 10)) / 100;
    var zPos = (Math.random() * 400) - 200;
    particle.setPosition(xPos,yPos,zPos);
    this.worldItem.add(particle.getSceneObject());
    World.particles[particle.getSceneObject().uuid] = particle;
    this.particles[particle.getSceneObject().uuid] = particle;
    yPos += 400 + (Math.random() * 200);
  }

  this.lineMaterial = new THREE.LineBasicMaterial({
    color: 0xFFFFFF,
    transparent: true,
    opacity:0.3
  });

  this.drawLine();

  this.xVelocity = 2 + Math.random() * 2;
  this.originalXVelocity = this.xVelocity;
};

SegmentedLine.prototype.drawLine = function () {
  var geometry = new THREE.Geometry();
  if (this.line != undefined) {
    geometry = this.line.geometry;
  }
  var counter = 0;
  for (var id in this.particles) {
    var particle = this.particles[id];
    var position = particle.getPosition();
    geometry.vertices[counter] = new THREE.Vector3(position.x,position.y,position.z);
    counter++;
  }

  if (this.line == undefined) {
    this.line = new THREE.Line(geometry, this.lineMaterial);
    this.worldItem.add(this.line);
  }else{
    this.line.geometry = geometry;
    this.line.geometry.verticesNeedUpdate = true;
  }
}

SegmentedLine.prototype.rollOver = function(particle_id) {
  this.active = true;
  this.movementInc = 0;
  this.xVelocity = 0;
  var target = this.particles[particle_id];
  for (var id in this.particles) {
    var particle = this.particles[id];
    this.particlePositions[id] = particle.getPosition();
    particle.moveTo(
      target.getPosition().x,
      particle.getPosition().y,
      target.getPosition().z,
      7000,
      TWEEN.Easing.Elastic.Out
    );
  }
}


SegmentedLine.prototype.rollOut = function() {
  for (var id in this.particles) {
    var particle = this.particles[id];
    var targetPos = this.particlePositions[id];

    if (particle.active == true) {
      particle.rollOut();
    }
    this.active = false;
    //tween all the particles back to the position they left off
    particle.moveTo(
      targetPos.x,
      targetPos.y,
      targetPos.z,
      1000,
      TWEEN.Easing.Quadratic.Out,
      this.onRollOutComplete,
      this
    );
  }
}

SegmentedLine.prototype.onRollOutComplete = function() {
  this.movementInc = 0.01;
  if (this.velocityTween != undefined) this.velocityTween.stop();
  this.velocityTween = new TWEEN.Tween(this).delay(1000);
  this.velocityTween.to({xVelocity:this.originalXVelocity}, 1000);
  this.velocityTween.easing(TWEEN.Easing.Quadratic.Out);
  this.velocityTween.start();
}

SegmentedLine.prototype.getParticleById = function(uuid) {
  return this.particles[uuid];
}

SegmentedLine.prototype.update = function () {
  //
  if (!this.active && this.movementInc > 0) {
    //
    var particleNum = 0;
    for (var id in this.particles) {
      var particle = this.particles[id];
      var initialPos = particle.getInitialPos();
      var xPos = Math.sin(this.counter + ((particleNum/5) * (Math.PI * 2))) * initialPos.x;
      particle.setPosition(xPos,initialPos.y,initialPos.z);
      particleNum++;
    }

    this.worldItem.position.x += this.xVelocity;
    if (this.worldItem.position.x > 3000) {
      this.worldItem.position.x = -2500;
    }
    this.counter += this.movementInc;
  }
  //
  this.drawLine();
}