function Segment (startParticle, endParticle) {
  WorldItem.call(this);
  this.start = startParticle;
  this.end = endParticle;
  this.init();
}

Segment.prototype = new WorldItem();
 
Segment.prototype.init = function() {
  //draw a line from the start particle to the end particle
  this.material = new THREE.LineBasicMaterial({
    color: 0xFFFFFF,
    transparent: true,
    opacity: 0.5
  });

  this.update();

};

Segment.prototype.update = function() {
  var geometry = new THREE.Geometry();
  geometry.vertices.push(
    new THREE.Vector3(-10, 0, 0),
    new THREE.Vector3(0, 10, 0)
  );
}