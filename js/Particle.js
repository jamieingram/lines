function Particle (radius) {
  WorldItem.call(this);
  this.radius = radius;
  this.active = false;
  this.isMouseDown = true;
  this.init();
}
 
Particle.prototype = new WorldItem();

Particle.prototype.init = function() {

  if (Particle.dotTexture == undefined) {
    //declare static variables
    Particle.dotTexture = THREE.ImageUtils.loadTexture("img/dot.png");
  }

  if (Particle.material == undefined) {
    Particle.material = new THREE.MeshBasicMaterial({
      map: Particle.dotTexture,
      transparent: true,
      opacity: 0.3,
      depthWrite: false
    });
  }

  if (Particle.geometry == undefined) {
    Particle.geometry = new THREE.PlaneBufferGeometry(this.radius,this.radius);
  }

  this.worldItem = new THREE.Mesh(Particle.geometry, Particle.material);
  this.worldItem.rotation.y = 20 * Math.PI / 180;
};

Particle.prototype.rollOver = function() {
  this.active = true;
  var tween = new TWEEN.Tween(this.worldItem.scale);
  tween.to({x:100, y:100}, 200);
  tween.easing(TWEEN.Easing.Quadratic.Out);
  tween.start();
}


Particle.prototype.rollOut = function() {
  this.active = false;
  var tween = new TWEEN.Tween(this.worldItem.scale);
  tween.to({x:this.initialScale, y:this.initialScale}, 400);
  tween.easing(TWEEN.Easing.Quadratic.Out);
  tween.start();
}

Particle.prototype.mouseDown = function() {
  this.isMouseDown = true;
}

Particle.prototype.mouseUp = function() {
  this.isMouseDown = false;
}

Particle.prototype.drawCircle = function(context) {
  context.beginPath();
  context.arc( 0, 0, 0.2, 0, Math.PI * 2, true );
  context.fill();
}